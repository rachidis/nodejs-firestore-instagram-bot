
function getRandom(min, max,exclude=[]) {
    let random=Math.round(Math.random() * (max - min) + min);
    if(exclude.indexOf(random)>-1){
        return getRandom(min,max,exclude)
    }
    return random 
}



module.exports={
    getRandom:getRandom
}