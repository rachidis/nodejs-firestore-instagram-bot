const fs = require("fs");
const functions = require("./functions");
const shuffle = require("shuffle-array");

class InstagramBot {
  constructor() {
    this.firebase_db = require("./db");
    this.config = require("./config/puppeteer.json");
  }

  async initPuppeter() {
    console.log("Initializing...");
    const puppeteer = require("puppeteer");
    const args = [
      "--no-sandbox",
      "--lang=en-us,en",
      "--disable-setuid-sandbox",
      "--disable-infobars",
      "--window-position=0,0",
      "--ignore-certifcate-errors",
      "--ignore-certifcate-errors-spki-list",
      '--user-agent="Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36"'
    ];
    const options = {
      args,
      headless: this.config.settings.headless,
      ignoreHTTPSErrors: true,
      userDataDir: "./tmp"
    };

    try {
      this.browser = await puppeteer.launch(options);
    } catch (error) {
      console.log("Couldn't Start ", error);
    }
    this.page = (await this.browser.pages())[0];
    await this.page.setExtraHTTPHeaders({
      "Accept-Language": "En"
    });
    const preloadFile = fs.readFileSync("./preload.js", "utf8");
    await this.page.evaluateOnNewDocument(preloadFile);
    this.page.setViewport({ width: 1280, height: 617 });

    await this.page.goto("https://bot.sannysoft.com");
    await this.page.screenshot({ path: "lastbotTest.png", fullPage: true });
    await this.page.waitFor(1000);
  }

  async visitInstagram() {
    console.log("visiting Instagram...");
    await this.page.goto(this.config.base_url, { timeout: 60000 });
    await this.page.mouse.move(1280 / 2, 617 / 2, { steps: 10 });
    await this.page.waitFor(2500);
    let login = await this.page.$(this.config.selectors.home_to_login_button);
    if (login) {
      console.log("login ...");
      await this.page.click(this.config.selectors.home_to_login_button);
      await this.page.waitFor(2500);
      /* Click on the username field using the field selector*/
      await this.page.click(this.config.selectors.username_field);
      await this.page.keyboard.type(this.config.username);
      await this.page.click(this.config.selectors.password_field);
      await this.page.keyboard.type(this.config.password);
      await this.page.click(this.config.selectors.login_button);
      await this.page.waitForNavigation();
    }
    await this.page.waitFor(2500);
    //Close Turn On Notification modal after login
    let closeModal = await this.page.$(this.config.selectors.not_now_button);
    if (closeModal) {
      await this.page.click(this.config.selectors.not_now_button);
    }
  }

  async likeComments() {
    //liking comments if there are any
    let moreComments = await this.page.$(".dCJp8.afkep");
    if (moreComments) {
      console.log("click more comments");
      await this.page.click(".dCJp8.afkep");
    }
    let commentsLength = await this.page.evaluate(x => {
      let element = document.querySelectorAll(x);
      return Promise.resolve(element ? element.length : "");
    }, this.config.selectors.post_comment_list);
    console.log(`There are  ${commentsLength} comments`);
    let maxCommentsTolike = functions.getRandom(1, 5);
    if (maxCommentsTolike > commentsLength) {
      maxCommentsTolike = commentsLength;
    }
    let exclude = [];
    console.log(`will like ${maxCommentsTolike} of the recent comments`);
    for (let r = 0; r < maxCommentsTolike; r++) {
      let randomSelector = functions.getRandom(2, commentsLength, exclude);
      exclude.push(randomSelector);
      console.log("liking comment N", randomSelector - 1);
      let commentSelector = `div.EtaWk > ul > ul:nth-child(${randomSelector}) > div > li > div > span > div > button`;
      this.page.click(commentSelector);
      await this.page.waitFor(functions.getRandom(10000, 20000));
    }
  }

  async visitHashtagUrl() {
    let hashTags = shuffle(this.config.hashTags);
    // loop through hashTags
    console.log(hashTags.length);
    for (let tagIndex = 0; tagIndex < hashTags.length; tagIndex++) {
      await this.unFollowUsers(3);
      console.log("<<<< Currently Exploring >>>> #" + hashTags[tagIndex]);
      //visit the hash tag url
      await this.page.click(this.config.selectors.searchField);
      await this.page.keyboard.type("#" + hashTags[tagIndex], { delay: 100 });
      await this.page.waitForNavigation();
      await this.page.click(this.config.selectors.searchFirstElement);
      await this.page.waitForNavigation();
      await this.page.keyboard.down("Enter");
      await this._doPostLikeAndFollow(
        this.config.selectors.hash_tags_base_class,
        this.page
      );  
    }
  }

  async _doPostLikeAndFollow(parentClass, page) {
    console.log("Posts Like And Follow");
    for (let r = 1; r < 4; r++) {
      //loops through each row
      for (let c = 1; c < 4; c++) {
        //loops through each item in the row
        let br = false;
        //Try to select post
        await this.page
          .click(
            `${parentClass} > div > .Nnq7C:nth-child(${r}) > .v1Nh3:nth-child(${c}) > a`
          )
          .catch(e => {
            console.log(e.message);
            br = true;
          });
        if (br) {
          console.log(
            "coudn't click on the post in the list. getting next Post ..."
          );
          continue;
        }

        //Wait for random amount of time after clicking post
        await this.page.waitFor(
          2250 + Math.floor(Math.random() * this.config.settings.action_delay)
        );

        //get the username of the current post
        let username = await this.page.evaluate(x => {
          let element = document.querySelector(x);
          return Promise.resolve(element ? element.innerHTML : "");
        }, this.config.selectors.post_username);
        console.log(`INTERACTING WITH ${username}'s POST`);

        await this.likeComments();
        await this.likePost();
        await this.followUser(username);
        
        //Closing the current post modal
        await this.page
          .click(this.config.selectors.post_close_button)
          .catch(e => console.log("<<< ERROR CLOSING POST >>> " + e.message));
        //Wait for random amount of time
        await this.page.waitFor(
          2250 + Math.floor(Math.random() * this.config.settings.action_delay)
        );
      }
    }
  }

  async followUser(username){
    if (username) {
      //let's check from our archive if we've follow this user before;
      let isArchivedUser = await this.firebase_db.inHistory(username);
      console.log("already followed: " + isArchivedUser);
      //get the current status of the current user using the text content of the follow button selector
      let followStatus = await this.page.evaluate(x => {
        let element = document.querySelector(x);
        return Promise.resolve(element ? element.innerHTML : "");
      }, this.config.selectors.post_follow_link);
      // If the text content of followStatus selector is Follow and we have not follow this user before
      // Save his name in the list of user we now follow and follow him, else log that we already follow him
      // or show any possible error
      const willfollow = followStatus === "Follow" && !isArchivedUser;
      const followRatio = Math.random() < this.config.settings.follow_ratio;
      console.log("Current status ", followStatus);
      if (willfollow && followRatio) {
        console.log("following:" + username);
        await this.firebase_db.addFollowing(username).catch(e => {
          console.log("<<< POSSIBLE ERROR >>>" + username + ":" + e.message);
        });
        await this.page.click(this.config.selectors.post_follow_link);
        await this.page.waitFor(
          5000 + Math.floor(Math.random() * this.config.settings.action_delay)
        );
      }
    }
  }
  async likePost() {
    //like the post if not already liked. Check against our like ratio so we don't just like all post
    //get the current post like status by checking if the selector exist
    let canLike = await this.page.$(this.config.selectors.post_heart_grey);
    let likeRatio = Math.random() < this.config.settings.like_ratio;
    console.log(
      "canLike? ",
      (canLike ? true : false) +
        " Ratio: " +
        this.config.settings.like_ratio +
        " = " +
        likeRatio
    );
    if (canLike && likeRatio) {
      console.log("I like it!...");
      await this.page.click(this.config.selectors.post_like_button); //click the like button
    }
    await this.page.waitFor(
      5000 + Math.floor(Math.random() * this.config.settings.action_delay)
    ); // wait for random amount of time.

  }
  /**
   * 
   * @param max max number of profiles to unfollow
   */
  async unFollowUsers(max=1) {
    let date_range =
      new Date().getTime() -
      this.config.settings.unfollow_after_days * 86400000;

    // get the list of users we are currently following
    let following = await this.firebase_db.getFollowings();
    let users_to_unfollow = [];
    if (following) {
      following.forEach(doc => {
        if (doc.exists) {
          let data = doc.data();
          let unfollow = data.added < date_range;
          if (unfollow) {
            users_to_unfollow.push(data.id);
          }
        }
      });
    }
    users_to_unfollow = shuffle(users_to_unfollow);
    if (users_to_unfollow.length) {
      if(max>users_to_unfollow.length){
        max=users_to_unfollow.length;
      }
      for (let n = 0; n < max; n++) {
        let user = users_to_unfollow[n];
        console.log("unfollowing: " + user);
          
        await this.page.click(this.config.selectors.searchField);
        await this.page.keyboard.type("@" + user, { delay: 100 });
        await this.page.waitForNavigation();
        await this.page.click(this.config.selectors.searchFirstElement);
        
        // await this.page.goto(`${this.config.base_url}/${user}/?hl=en`);
        await this.page.waitFor(3000);
        let followStatus = await this.page.evaluate(x => {
          let element = document.querySelector(x);
          return Promise.resolve(element ? element.innerHTML : "");
        }, this.config.selectors.user_unfollow_button);
        if(!followStatus){
          console.warn('conceling unfollows, Instagram getting doubts!...');
          await this.page.click('.logo a');
          await this.page.waitFor(5000);
          break; 
        }
        if (followStatus === "Following") {
          console.log("<<< UNFOLLOW USER >>>" + user);
          //click on unfollow button
          await this.page.click(this.config.selectors.user_unfollow_button);
          //wait for a sec
          await this.page.waitFor(5000);
          //confirm unfollow user
          await this.page.click(
            this.config.selectors.user_unfollow_confirm_button
          );
          //wait for random amount of time
          await this.page.waitFor(5000);
          //remove user to our following history
          await this.firebase_db.unFollow(user);
        } else {
          //remove user to our following history
          await this.firebase_db.unFollow(user);
        }
      }
    }
  }
  async closeBrowser() {
    await this.browser.close();
  }
}

module.exports = InstagramBot;
