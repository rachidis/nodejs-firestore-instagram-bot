var admin = require("firebase-admin");
var serviceAccount = require("./config/serviceAccountKey.json");
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://myinstabot-2e277.firebaseio.com"
});

let database = admin.firestore();
const settings = {/* your settings... */ timestampsInSnapshots: true};
database.settings(settings);
const followingCol = () => database.collection('following');
const followHistoryCol = () => database.collection('follow_history');
let addFollowing = async id =>{
    const added = new Date().getTime();
    followHistoryCol().add({id,added})
    return followingCol().add({id,added});
};
let getFollowings = async () => followingCol().get();
let unFollow = async username => (await followingCol().where('id',"==",username).get()).forEach(snap=>snap.ref.delete());
let inHistory = async username => (await followHistoryCol().where('id',"==",username).get()).size>0;

module.exports = {
  addFollowing,
  getFollowings,
  unFollow,
  inHistory
};