[![LinkedIn](https://img.shields.io/badge/Rachid_Ismaili-LinkedIn-blue.svg)](https://www.linkedin.com/in/rachid-ismaili/)

 # Instagram nodeJs Bot
 
Instagram bot made using Nodejs puppeteer & firebase. 
it is not running in headless mode. It can be changed in puppeteer.json file

 
  # install & run.
  
run command inside bot Folder
``` bash
npm i
```


Edit DB files
    
*  /bot/db.js & /bot/config/(db_config.json & serviceAccountKey.json): contains firestore project credential
        (it's ok to make its rules locked as the bot is making changes via firebase-admin which has already all previleges)
*  -/bot/config/puppeteer.json: Edit IG account login details, targeted hashtags & setting.


run command inside bot Folder
``` bash
node index.js
```






